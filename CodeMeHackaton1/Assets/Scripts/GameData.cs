﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData
{

    public int Points;
    static public int HighScore;

    public int Lives;
    public const int START_LIVES = 7;

    public static List<string> Levels = new List<string>() { "MainMenu", "Level1", "Level2", "Level3", "Level4"};

    public int CurrentLevelIndex;

}
