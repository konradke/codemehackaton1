﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionHandler : MonoBehaviour
{

    private GameObject collectedItem;

    private void Start()
    {
        GameManager.Instance.CoinHit += DestroyOther;
        GameManager.Instance.CheckPointHit += DestroyOther;
        GameManager.Instance.WinHit += RemovePlayer;
    }

    private void OnDestroy()
    {
        GameManager.Instance.CoinHit -= DestroyOther;
        GameManager.Instance.CheckPointHit -= DestroyOther;
        GameManager.Instance.WinHit -= RemovePlayer;
    }

    public void PlayerCollided(Collider2D other)
    {
        collectedItem = other.gameObject;

        switch (other.tag)
        {
            default:
                break;

            case "enemy":
                EnemyCollision();
                break;

            case "coin":
                CoinCollision(collectedItem);
                break;

            case "checkpoint":
                CheckPointCollision(collectedItem);
                break;

            case "WinPoint":
                WinCollision();
                break;
            
        }
    }


    // trigger related event

    private void EnemyCollision()
    {
        GameManager.Instance.OnEnemyHit();
    }

    private void CoinCollision(GameObject token)
    {
     
        GameManager.Instance.OnCoinHit(token);
    }

    private void CheckPointCollision(GameObject token)
    {
        GameManager.Instance.OnCheckPointHit(token);
    }

    private void WinCollision()
    {
        GameManager.Instance.OnWinHit();
    }

    // destroy collected object?
    private void DestroyOther(GameObject token)
    {
        if (collectedItem != null)
            Destroy(token);
    }

    private void RemovePlayer()
    {
        Destroy(this);
    }
}
