﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour


{
    public GameObject GOScreen;

    [SerializeField]
    private Transform ButtonHolder;

    public LevelButton ButtonPrefab;

    private void Start()
    {
        GameManager.Instance.GameOver += GameOverScreen;


        for (int i = GameData.Levels.Count-1; i > 0; i--)
        {
            GameObject SceneButton = Instantiate(ButtonPrefab.gameObject, ButtonHolder);
            SceneButton.GetComponent<LevelButton>().ButtonIndex = i;

            Transform SceneButtonT = SceneButton.GetComponent<Transform>();
            SceneButtonT.SetAsFirstSibling();
        }
 
    }

    private void OnDestroy()
    {
        GameManager.Instance.GameOver -= GameOverScreen;

    }
        void GameOverScreen()
    {
        GOScreen.SetActive(true);
    }



}
