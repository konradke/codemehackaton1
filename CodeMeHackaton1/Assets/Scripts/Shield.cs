﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{

    private float sizeSpeed = 280f;
    public Vector3 StartSize = new Vector3(0.07f, 0.007f, 0.07f);
    private bool isResized = false;

    public static int EnemyCount;

    private float mySpeed;


    private void Start()
    {
        transform.localScale = StartSize;
  //      mySpeed = GameManager.Instance.EnemySpeed;
     //   transform.SetPositionAndRotation(transform.position, Quaternion.Euler(90f, 0, 45f));
    }


    void Update()
    {
   //    transform.GetComponentInChildren<Transform>().Rotate(Vector3.up * Time.deltaTime*280f);

        if (!isResized)
        {
            transform.localScale += StartSize * Time.deltaTime * sizeSpeed;
            if (transform.localScale.x >= 0.5f)
            {
                isResized = true;
            }
        }
        transform.Translate(Vector3.back * Time.deltaTime * mySpeed);

        //if (transform.position.z < GameManager.BottomSize - 5f)
        //{
        //    Destroy(gameObject);
        //}
    }

    void OnTriggerEnter(Collider other)
    {
       
        if (other.gameObject.tag == "Player")
        {
            Destroy(gameObject);

            Lives thisLives = FindObjectOfType<Lives>();
      
   //         if (GameManager.Instance.Lives < 4)
            {
  //              thisLives.LifeGained();
 //               GameManager.Instance.Lives++;
                
            }

        }
    }


}