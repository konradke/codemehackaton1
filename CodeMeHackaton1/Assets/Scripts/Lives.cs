﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lives : MonoBehaviour
{
    [SerializeField]
    private List<Image> UILivesImages;

    public Image UILifePrefab;

    public Transform UILifePosition;

    private void Start()
    {
        GameManager.Instance.EnemyHit += LifeLost;
        GameManager.Instance.GameStart += LifeRestored;

        if(LifeCount()!=UILivesImages.Count)
        {
            LifeRestored();
        }
    }

    private void OnDestroy()
    {
        GameManager.Instance.EnemyHit -= LifeLost;
        GameManager.Instance.GameStart -= LifeRestored;
    }


    private int LifeCount()
    {
        return GameManager.SessionData.Lives;
    }

    private void LifeLost()
    {
        int index = UILivesImages.Count - 1;
        Image img = UILivesImages[index];
        UILivesImages.RemoveAt(index);
        Destroy(img.gameObject);
    }

    private void LifeGained()
    {
        Image img = Instantiate(UILifePrefab, UILifePosition);
        UILivesImages.Add(img);
    }

    private void LifeRestored()
    {
        for (int i = 0; i < LifeCount(); i++)
        {
            LifeGained();
        }
        
    }
}
