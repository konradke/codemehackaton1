﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    static public bool IsGameOver = false;

    public delegate void PlayerCollisionEnemy();
    public event PlayerCollisionEnemy EnemyHit;

    public delegate void PlayerCollisionToken(GameObject token);
    public event PlayerCollisionToken CoinHit;

    public event PlayerCollisionToken CheckPointHit;

    public delegate void PlayerCollisionVictory();
    public event PlayerCollisionVictory WinHit;

    public delegate void GameEvent();
    public event GameEvent GameStart;
    public event GameEvent GameOver;

    public static GameManager Instance;

    [SerializeField]
    public static GameData SessionData;

   
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;       
        }
        else
        {
            Destroy(gameObject);
        }
        CreateGameData();

        DontDestroyOnLoad(gameObject);

        Instance.GameStart += ResetLives;
        Instance.GameStart += CreateGameData;
        Instance.EnemyHit += RemoveLives;
        Instance.WinHit += LevelComplete;
        Instance.CoinHit += AddPoints;
    }

    private void OnDestroy()
    {
      
        Instance.GameStart -= CreateGameData;
        Instance.EnemyHit -= RemoveLives;
        Instance.WinHit -= LevelComplete;
        Instance.CoinHit -= AddPoints;
    }

    //GameData methods
    private void CreateGameData()
    {
        if (SessionData == null)
        {
            SessionData = new GameData();
        }
    }

    //Level methods
    public void StartLevel(int index)
    {
        GameManager.Instance.OnGameStart();
        GoToLevel(index);
        GameManager.SessionData.CurrentLevelIndex = index;
    }

    public void LevelComplete()
    {
        if (SessionData.CurrentLevelIndex < GameData.Levels.Count - 1)
        {
            GoToLevel(++SessionData.CurrentLevelIndex);
        }
        else 
        {
            OnGameOver();
        }
    }

    public void GoToLevel(int levelIndex)
    {
        SceneManager.LoadScene(GameData.Levels[levelIndex]);
    }

    //Lives methods
    public void AddLives()
    {
        SessionData.Lives++;
    }

    public void RemoveLives()
    {
        SessionData.Lives--;

        if (SessionData.Lives <=0)
        {
            OnGameOver();
        }
    }

    public void ResetLives()
    {
        SessionData.Lives = GameData.START_LIVES;
    }

    //points methods
    public void AddPoints(GameObject token)
    {
        CoinValue tokenWorth = token.GetComponent<CoinValue>();
        int points = tokenWorth.Value;

        SessionData.Points += points;
        Debug.Log(SessionData.Points);

    }

    public void ResetPoints()
    {
        SessionData.Points = 0;
    }

    //GameOver methods

    public void OnGameOver()
    {
 //       GameOver();

        GoToLevel(0);
        GameData.HighScore = SessionData.Points;
        SessionData = null;
        Destroy(gameObject);
    }


    //public event handlers

    public void OnEnemyHit()
    {
        Instance.EnemyHit?.Invoke();
    }

    public void OnCoinHit(GameObject token)
    {
        Instance.CoinHit?.Invoke(token);
    }

    public void OnCheckPointHit(GameObject token)
    {
        Instance.CheckPointHit?.Invoke(token);
    }

    public void OnWinHit()
    {
        Instance.WinHit?.Invoke();
    }

    public void OnGameStart()
    {
        Instance.GameStart?.Invoke();
    }
}

