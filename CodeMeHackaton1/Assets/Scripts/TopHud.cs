﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopHud : MonoBehaviour
{
    public static TopHud Hud;
    private void Awake()
    {
        if (Hud == null)
        {
            Hud = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
}
