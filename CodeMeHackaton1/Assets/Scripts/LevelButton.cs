﻿using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public int ButtonIndex;
    private Text ButtonText;

    void Start()
    {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
        ButtonText = GetComponentInChildren<Text>();
        ButtonText.text = GameData.Levels[ButtonIndex];

    }
    void TaskOnClick()
    {
        GameManager.Instance.StartLevel(ButtonIndex);
    }
}