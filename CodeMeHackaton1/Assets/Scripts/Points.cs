﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Points : MonoBehaviour
{
    private Text ScoreHolder;
    private string points;
    void Start()
    {
        ScoreHolder = GetComponent<Text>();
        GameManager.Instance.CoinHit += UpdatePoints;
        UpdatePoints(null);
    }

    void OnDestroy()
    {
        GameManager.Instance.CoinHit -= UpdatePoints;
    }

    private void UpdatePoints(GameObject Token)
    {
        int pts = GameManager.SessionData.Points;
        points = pts.ToString("0000000000");

        ScoreHolder.text = points;
    }
}
