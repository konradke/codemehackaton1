﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    Rigidbody2D rigidbody;

    public float Speed;
    public float JumpHeight;
    public Transform RespawnPosition;

    public Vector3 currentRespawnVector;

    [SerializeField]
    private CollisionHandler playerCollisions;

    public AudioSource JumpAudio;

    private void Start()
    {

        rigidbody = GetComponent<Rigidbody2D>();

  //      currentRespawnVector = RespawnPosition.position;

        GameManager.Instance.EnemyHit += PlayerRespawn;
        GameManager.Instance.CheckPointHit += NewRespawnPosition;
        GameManager.Instance.WinHit += RemovePlayer;

    }

    private void OnDestroy()
    {
        GameManager.Instance.EnemyHit -= PlayerRespawn;
        GameManager.Instance.CheckPointHit -= NewRespawnPosition;
        GameManager.Instance.WinHit -= RemovePlayer;
    }

    void Update()
    {
        float movement = Input.GetAxis("Horizontal");

        transform.Translate(Vector2.right * movement * Time.deltaTime * Speed);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbody.AddRelativeForce(Vector2.up * JumpHeight, ForceMode2D.Impulse);
       //     JumpAudio.Play();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        playerCollisions.PlayerCollided(other);
    }

    private void PlayerRespawn()
    {
        LevelController.ThePlayer.transform.position = currentRespawnVector;
    }
   
    private void NewRespawnPosition(GameObject respawnPoint)
    {
        Transform respawnPointPosition = respawnPoint.GetComponent<Transform>();
        currentRespawnVector = new Vector3(respawnPointPosition.position.x, respawnPointPosition.position.y + 1f, respawnPointPosition.position.z);
    }

    private void RemovePlayer()
    {
        Destroy(this);
    }
}

